FROM node

ENV HOME=/app
RUN mkdir /app

ADD package.json $HOME

WORKDIR $HOME

RUN npm i -g @adonisjs/cli

COPY init.sh /init.sh
RUN chmod 755 /init.sh

CMD ["/init.sh"]
