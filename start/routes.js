'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('welcome')
Route.post('/api/v1/auth/login', 'AuthController.login')
Route.get('/api/v1/articles/available', 'ArticleController.available')
Route.get('/api/v1/articlereservations', 'ArticleReservationController.index')
Route.post('/api/v1/articlereservations', 'ArticleReservationController.store')
  .validator('ArticleReservation/StoreArticleReservation')
Route.group(() => {

  Route.resource('properties', 'PropertyController')
    .apiOnly()
    .middleware(['company'])
    .validator(new Map([
      [['properties.show'], ['Property/GetProperty']],
      [['properties.store'], ['Property/StoreProperty']],
      [['properties.update'], ['Property/UpdateProperty']],
      [['properties.destroy'], ['Property/GetProperty']]
    ]))

  Route.resource('property_values', 'PropertyValueController')
    .apiOnly()
    .middleware(['company'])
    .validator(new Map([
      [['property_values.show'], ['PropertyValue/GetPropertyValue']],
      [['property_values.store'], ['PropertyValue/StorePropertyValue']],
      [['property_values.update'], ['PropertyValue/UpdatePropertyValue']],
      [['property_values.destroy'], ['PropertyValue/GetPropertyValue']]
    ]))

  Route.resource('products', 'ProductController')
    .apiOnly()
    .middleware(['company'])
    .validator(new Map([
      [['products.show'], ['Product/GetProduct']],
      [['products.store'], ['Product/StoreProduct']],
      [['products.update'], ['Product/UpdateProduct']],
      [['products.destroy'], ['Product/GetProduct']]
    ]))

  Route.resource('articles', 'ArticleController')
    .apiOnly()
    .middleware(['company'])
    .validator(new Map([
      [['articles.show'], ['Article/GetArticle']],
      [['articles.store'], ['Article/StoreArticle']],
      [['articles.update'], ['Article/UpdateArticle']],
      [['articles.destroy'], ['Article/GetArticle']]
    ]))
}).prefix('api/v1').middleware(['auth'])

