const {ServiceProvider} = require('@adonisjs/fold')


class ExtendedValidatorProvider extends ServiceProvider {
  register() {
    //
  }

  async _uniqueFn(data, field, message, args, get) {

    const Database = use('Database')
    const util = require('util')
    let ignoreId = null
    const fields = args[1].split('/')
    const table = args[0]
    if (args[2]) {
      ignoreId = args[2]
    }

    const rows = await Database.table(table).where((builder) => {
      for (const f of fields) {
        builder.where(f, '=', get(data, f))
      }
      if (ignoreId) {
        builder.whereNot('id', '=', ignoreId)
      }
    }).count('* as total')

    if (rows[0].total) {
      throw message
    }
  }

  async _nullableFn(data, field, message, args, get) {
    const value = get(data, field)

    if (!(field in data)) {
      return;
    }


    if (!value) {
      message = `'${field}' can't be empty.`
      throw message
    }
  }

  async _afterOrEqual(data, field, message, args, get) {
    let value = new Date(get(data, field));
    if (!value instanceof Date) {
      throw message;
    }
    let dateToCompare = args[0];
    if (!dateToCompare) {
      throw message;
    }
    if (dateToCompare === "now") {
      dateToCompare = new Date()
    } else if (dateToCompare === "curdate") {
      dateToCompare = new Date()
      dateToCompare.setHours(0, 0, 0, 0)
    } else {
      dateToCompare = new Date(dateToCompare);
    }
    if (!dateToCompare instanceof Date) {
      throw message;
    }
    if (value < dateToCompare) {
      throw message;
    }

  }

  async _recordExists(data, field, message, args, get) {
    const Database = use('Database')
    const util = require('util')

    const [table, ...params] = args;
    let fields = [];

    params.forEach(function (val, i) {
      if (i % 2 === 1) return; // Skip all even elements (= odd indexes)
      fields[val] = params[i + 1];   // Assign the next element as a value of the object,
    });
    const rows = await Database.table(table).where(fields).count('* as total');
    if (!rows[0].total) {
      throw message
    }
  }

  async _propertyValueExists(data, field, message, args, get) {
    const Database = use('Database')

    let value = get(data, field)

    if (!value) {
      /**
       * skip validation if value is not defined. `required` rule
       * should take care of it.
       */
      return false;
    }

    let rows = await Database.table('property_values')
      .count('* as total')
      .innerJoin('properties', function () {
        this
          .on('properties.id', 'property_values.property_id')
          .andOn('properties.company_id', data.company_id)
      })
      .where('property_values.id', Array.isArray(value) ? 'in' : '=', value)
    if (rows[0]['total'] === 0) {
      throw message
    }
  }

  async _articleExists(data, field, message, args, get) {
    const Database = use('Database')

    let value = get(data, field)

    if (!value) {
      /**
       * skip validation if value is not defined. `required` rule
       * should take care of it.
       */
      return false;
    }

    let rows = await Database.table('articles')
      .count('* as total')
      .innerJoin('products', function () {
        this
          .on('products.id', 'articles.product_id')
          .andOn('products.company_id', data.company_id)
      })
      .where('articles.id', value)
    if (rows[0]['total'] === 0) {
      throw message
    }
  }


  boot() {
    const Validator = use('Validator')

    Validator.extend('uniqueCompound', this._uniqueFn, 'Must be unique')
    Validator.extend('nullable', this._nullableFn, '')
    Validator.extend('afterOrEqual', this._afterOrEqual, 'Incorrect date')
    Validator.extend('recordExists', this._recordExists, 'That record does not exist.')
    Validator.extend('propertyValueExists', this._propertyValueExists, 'That property value does not exist.')
    Validator.extend('articleExists', this._articleExists, 'That article does not exist.')
  }
}

module.exports = ExtendedValidatorProvider
