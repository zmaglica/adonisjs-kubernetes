'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class Company {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request, auth }, next) {
    const company_id = auth.user.company_id
    if (!company_id) {
      throw new ForbiddenException()
    }
    request._all.company_id = company_id;
    await next()
  }
}

module.exports = Company
