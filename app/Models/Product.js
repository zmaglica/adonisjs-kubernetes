'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Product extends Model {

  static boot() {
    super.boot()

    this.addTrait('@provider:Lucid/SoftDeletes')
  }

  company() {
    return this.belongsTo('App/Models/Company')
  }

  articles() {
    return this.hasMany('App/Models/Article');
  }
}

module.exports = Product
