'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PropertyValue extends Model {
  property() {
    return this.belongsTo('App/Models/Property')
  }

  articles() {
    return this.belongsToMany('App/Models/Article').pivotTable('article_property_values')
  }
}

module.exports = PropertyValue
