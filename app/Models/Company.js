'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Company extends Model {

  static boot() {
    super.boot()

    this.addTrait('@provider:Lucid/SoftDeletes')
  }

  users() {
    return this.hasMany('App/Models/User');
  }

  products() {
    return this.hasMany('App/Models/Product');
  }

  properties() {
    return this.hasMany('App/Models/Property');
  }
  propertyValues () {
    return this.manyThrough('App/Models/Property', 'propertyValues', 'id', 'id')
  }
}

module.exports = Company
