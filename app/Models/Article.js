'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Article extends Model {

  static boot() {
    super.boot()

    this.addTrait('@provider:Lucid/SoftDeletes')
  }

  product() {
    return this.belongsTo('App/Models/Product')
  }

  propertyValues() {
    return this.belongsToMany('App/Models/PropertyValue').pivotTable('article_property_values')
  }
}

module.exports = Article
