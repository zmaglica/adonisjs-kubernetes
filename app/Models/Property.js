'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Property extends Model {

  static boot() {
    super.boot()

    this.addTrait('@provider:Lucid/SoftDeletes')
  }

  propertyValues() {
    return this.hasMany('App/Models/PropertyValue');
  }

  company() {
    return this.belongsTo('App/Models/Company')
  }
}

module.exports = Property
