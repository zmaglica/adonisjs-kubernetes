'use strict'

class GetProperty {
  get rules () {
    const companyId = this.ctx.request.input('company_id')
    const id = this.ctx.params.id
    return {
      id: `required|recordExists:properties,id,${id},company_id,${companyId}`
    }
  }
  get data () {
    const requestBody = this.ctx.request.all()
    const id = this.ctx.params.id

    return Object.assign({}, requestBody, { id })
  }
  get messages () {
    return {
      'id.required': 'You must provide a property id.',
      'id.recordExists': 'Property doesn\'t exists',
    }
  }
}

module.exports = GetProperty
