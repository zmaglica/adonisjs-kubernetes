'use strict'

class UpdateProperty {
  get rules () {
    const companyId = this.ctx.request.input('company_id')
    const id = this.ctx.params.id
    return {
      name: `required|recordExists:properties,id,${id},company_id,${companyId}|uniqueCompound:properties,name/company_id,${companyId}`,
    }
  }
  get messages () {
    return {
      'name.required': 'You must provide a property name.',
      'name.uniqueCompound': 'Property with that name already exists',
      'name.recordExists': 'Property doesn\'t exists',
    }
  }
}

module.exports = UpdateProperty
