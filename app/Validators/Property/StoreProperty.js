'use strict'

class StoreProperty {
  get rules () {
    return {
      name: 'required|uniqueCompound:properties,name/company_id'
    }
  }
  get messages () {
    return {
      'name.required': 'You must provide a property name.',
      'name.uniqueCompound': 'Property with that name already exists',
    }
  }
}

module.exports = StoreProperty
