'use strict'

class StoreArticleReservation {
  get rules () {
    const articleId = this.ctx.request.input('article_id')
    return {
      article_id: `required|recordExists:articles,id,${articleId}`,
      picking_date: `required|dateFormat:YYYY-MM-DD|afterOrEqual:curdate`
    }
  }
  get messages () {
    return {
      'article_id.required': 'You must provide a article id.',
      'article_id.recordExists': 'Article with that id doesn\'t exists',
      'picking_date.required': 'You must provide a picking date.',
    }
  }
}

module.exports = StoreArticleReservation
