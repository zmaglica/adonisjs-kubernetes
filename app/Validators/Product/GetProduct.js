'use strict'

class GetProduct {
  get rules () {
    const companyId = this.ctx.request.input('company_id')
    const id = this.ctx.params.id
    return {
      id: `required|recordExists:products,id,${id},company_id,${companyId}`
    }
  }
  get data () {
    const requestBody = this.ctx.request.all()
    const id = this.ctx.params.id

    return Object.assign({}, requestBody, { id })
  }
  get messages () {
    return {
      'id.required': 'You must provide a product id.',
      'id.recordExists': 'Product doesn\'t exists',
    }
  }
}

module.exports = GetProduct
