'use strict'

class StoreProduct {
  get rules () {
    return {
      name: 'required|uniqueCompound:products,name/company_id',
      description: 'required'
    }
  }
  get messages () {
    return {
      'name.required': 'You must provide a product name.',
      'name.uniqueCompound': 'Product with that name already exists',
      'description.required': 'You must provide a product description.',
    }
  }
}

module.exports = StoreProduct
