'use strict'

class UpdateProduct {
  get rules () {
    const companyId = this.ctx.request.input('company_id')
    const id = this.ctx.params.id
    return {
      name: `nullable|recordExists:products,id,${id},company_id,${companyId}|uniqueCompound:products,name/company_id,${companyId}`,
      description: 'nullable|string'
    }
  }
  get messages () {
    return {
      'name.uniqueCompound': 'Product with that name already exists',
      'name.recordExists': 'Product doesn\'t exists',
    }
  }
}

module.exports = UpdateProduct
