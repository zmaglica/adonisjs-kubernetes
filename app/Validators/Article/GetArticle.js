'use strict'

class GetArticle {
  get rules() {
    return {
      id: 'required|articleExists'
    }
  }

  get data() {
    const requestBody = this.ctx.request.all()
    const id = this.ctx.params.id

    return Object.assign({}, requestBody, {id})
  }

  get messages() {
    return {
      'id.required': 'You must provide a article id.',
      'id.articleExists': 'Article with that id doesn\'t exists',
    }
  }
}

module.exports = GetArticle
