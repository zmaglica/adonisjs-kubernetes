'use strict'

class UpdateArticle {
  get rules() {

    const companyId = this.ctx.request.input('company_id')
    const productId = this.ctx.request.input('product_id')
    console.log(this.ctx.request.all());
    return {
      id: 'required|articleExists',
      product_id: `nullable|recordExists:products,id,${productId},company_id,${companyId}`,
      name: 'nullable|string',
      sku: 'nullable|string',
      price: 'nullable|number|above:0',
      quantity: 'nullable|integer',
      property_value: 'nullable|min:1|max:10|propertyValueExists'
    }
  }

  get data() {
    const requestBody = this.ctx.request.all()
    const id = this.ctx.params.id

    return Object.assign({}, requestBody, {id})
  }

  get messages() {
    return {
      'id.required': 'You must provide a article id.',
      'id.articleExists': 'Article with that id doesn\'t exists',
      'product_id.recordExists': 'Product with that id doesn\'t exists',
      'price.number': 'Article price must be number',
      'price.above': 'Article price must be greater than 0',
      'quantity.integer': 'Article quantity must be integer',
      'property_value.min': 'Minimum 1 Property value is required',
      'property_value.max': 'Maximum of 10 Property values are allowed',
      'property_value.propertyValueExists': 'Submitted product values doesn\'t exists',
    }
  }
}

module.exports = UpdateArticle
