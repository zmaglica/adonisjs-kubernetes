'use strict'

class StoreArticle {
  get rules() {

    const companyId = this.ctx.request.input('company_id')
    const productId = this.ctx.request.input('product_id')
    const id = this.ctx.params.id

    let rules = {
      product_id: `required|recordExists:products,id,${productId},company_id,${companyId}`,
      name: 'required',
      sku: 'required',
      price: 'required|number|above:0',
      quantity: 'required|integer',
      property_value: 'required|min:1|max:10|propertyValueExists'
    }

    if (id) {
      rules.id = 'required|articleExists'
    }

    return rules
  }

  get data() {
    const requestBody = this.ctx.request.all()
    const id = this.ctx.params.id
    if (id) {
      return Object.assign({}, requestBody, {id})
    }
  }

  get messages() {
    return {
      'id.required': 'You must provide a article id.',
      'id.articleExists': 'Article with that id doesn\'t exists',
      'product_id.required': 'You must provide a product id.',
      'product_id.recordExists': 'Product with that id doesn\'t exists',
      'name.required': 'You must provide a property name.',
      'sku.required': 'You must provide sku.',
      'price.required': 'Article price is missing',
      'price.number': 'Article price must be number',
      'price.above': 'Article price must be greater than 0',
      'quantity.required': 'Article quantity is missing',
      'quantity.integer': 'Article quantity must be integer',
      'property_value.required': 'Property value is missing',
      'property_value.min': 'Minimum 1 Property value is required',
      'property_value.max': 'Maximum of 10 Property values are allowed',
      'property_value.propertyValueExists': 'Submitted product values doesn\'t exists',
    }
  }
}

module.exports = StoreArticle
