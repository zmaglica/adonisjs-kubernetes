'use strict'

class StorePropertyValue {
  get rules () {
    const companyId = this.ctx.request.input('company_id')
    const propertyId = this.ctx.request.input('property_id')
    return {
      value: 'required|uniqueCompound:property_values,value/property_id',
      property_id: `required|recordExists:properties,id,${propertyId},company_id,${companyId}`
    }
  }
  get messages () {
    return {
      'value.required': 'You must provide a property value.',
      'value.uniqueCompound': 'Property with that value already exists',
      'property_id.required': 'You must provide a property id.',
      'property_id.recordExists': 'Property with that id doesn\'t exists',
    }
  }
}

module.exports = StorePropertyValue
