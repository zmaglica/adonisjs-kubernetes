'use strict'

class UpdatePropertyValue {
  get rules () {
    const id = this.ctx.params.id
    return {
      id: 'required|propertyValueExists',
      value: `required|uniqueCompound:property_values,value/id,${id}`,
    }
  }
  get data () {
    const requestBody = this.ctx.request.all()
    const id = this.ctx.params.id

    return Object.assign({}, requestBody, { id })
  }
  get messages () {
    return {
      'value.required': 'You must provide a property value.',
      'value.uniqueCompound': 'Property with that value already exists',
      'id.required': 'You must provide a property value id',
      'id.propertyValueExists': 'Property doesn\'t exists',
    }
  }
}

module.exports = UpdatePropertyValue
