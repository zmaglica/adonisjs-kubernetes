'use strict'

class GetPropertyValue {
  get rules () {
    return {
      id: 'required|propertyValueExists'
    }
  }
  get data () {
    const requestBody = this.ctx.request.all()
    const id = this.ctx.params.id

    return Object.assign({}, requestBody, { id })
  }
  get messages () {
    return {
      'id.required': 'You must provide a property value id',
      'id.propertyValueExists': 'Property doesn\'t exists',
    }
  }
}

module.exports = GetPropertyValue
