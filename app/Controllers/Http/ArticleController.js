'use strict'
const Article = use('App/Models/Article')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */

/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/**
 * Resourceful controller for interacting with articles
 */
class ArticleController {
  /**
   * Show a list of all articles.
   * GET articles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({request, auth, response}) {
    let articles = await Article
      .query()
      .select('articles.*')
      .innerJoin('products', function () {
        this
          .on('products.id', 'articles.product_id')
          .andOn('products.company_id', request.input('company_id'))
      })
      .with('product')
      .with('propertyValues')
      .fetch();
    return response.json(articles);
  }


  /**
   * Create/save a new article.
   * POST articles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({request, response}) {
    let article = await Article.create(request.only(['product_id', 'name', 'sku', 'price', 'quantity']));
    if ((request.input('property_value'))) {
      await article.propertyValues().attach(request.input('property_value'));
    }
    return response.created();
  }

  /**
   * Display a single article.
   * GET articles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({params, request, response}) {
    let article = await Article.findOrFail(params.id);
    return response.json(article)
  }

  /**
   * Update articles.
   * PUT or PATCH articles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({params, request, response}) {
    let article = await Article.findOrFail(params.id);
    article.merge(request.only(['product_id', 'name', 'sku', 'price', 'quantity']));
    await article.save()
    if ((request.input('property_value'))) {
      await article.propertyValues().sync(request.input('property_value'));
    }
    return response.ok()

  }

  /**
   * Delete a article with id.
   * DELETE articles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({params, request, response}) {
    let article = await Article.findOrFail(params.id);
    await article.propertyValues().detach(); //TODO: Add listener on delete to avoid manually delete
    await article.delete();
    return response.ok();
  }

  /**
   * Show a list of all available articles.
   * GET articles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async available({request, auth, response}) {
    let articles = await Article
      .query()
      .where('quantity' , '>', 0)
      .fetch();
    return response.json(articles);
  }
}

module.exports = ArticleController
