'use strict'
const PropertyValue = use('App/Models/PropertyValue')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/**
 * Resourceful controller for interacting with property values
 */
class PropertyValueController {
  /**
   * Show a list of all property values.
   * GET property_values
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index ({ request, auth, response }) {
    let propertyValues = await PropertyValue
      .query()
      .select('property_values.*')
      .innerJoin('properties', function () {
        this
          .on('properties.id', 'property_values.property_id')
          .andOn('properties.company_id', request.input('company_id'))
      })
      .with('property')
      .fetch();
    return response.json(propertyValues);
  }


  /**
   * Create/save a new property value.
   * POST property_values
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request,  response }) {
    PropertyValue.create(request.only(['value', 'property_id']));
    return response.created();
  }

  /**
   * Display a single property value.
   * GET property_values/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show ({ params, request, response }) {
    let property_value = await PropertyValue.findOrFail(params.id);
    return response.json(property_value)
  }

  /**
   * Update property value details.
   * PUT or PATCH property_values/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    let propertyValue = await PropertyValue.findOrFail(params.id)
    propertyValue.merge(request.only(['value', 'property_id']))
    await propertyValue.save()
    return response.ok()

  }

  /**
   * Delete a property_values with id.
   * DELETE property_values/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    let propertyValue = await PropertyValue.findOrFail(params.id)
    await propertyValue.delete();
    return response.ok();
  }
}

module.exports = PropertyValueController
