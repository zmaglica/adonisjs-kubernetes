'use strict'
const Product = use('App/Models/Product')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */

/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/**
 * Resourceful controller for interacting with products
 */
class ProductController {
  /**
   * Show a list of all products.
   * GET products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({request, auth, response}) {
    return response.json(await auth.user.products().fetch());
  }


  /**
   * Create/save a new product.
   * POST products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({request, response}) {
    Product.create(request.only(['name', 'description', 'company_id']));
    return response.created();
  }

  /**
   * Display a single product.
   * GET product/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async show({params, request, response}) {
    let product = await Product.findOrFail(params.id);
    return response.json(product)
  }

  /**
   * Update product details.
   * PUT or PATCH products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({params, request, response}) {
    let product = await Product.findOrFail(params.id);
    product.merge(request.only(['name', 'description']));
    await product.save()
    return response.ok()

  }

  /**
   * Delete a product with id.
   * DELETE products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({params, request, response}) {
    let product = await Product.findOrFail(params.id);
    await product.delete();
    return response.ok();
  }
}

module.exports = ProductController
