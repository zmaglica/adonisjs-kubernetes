'use strict'
const ArticleReservation = use('App/Models/ArticleReservation')
const ArticleReservationService = use('App/Services/ArticleReservationService')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */

/** @typedef {import('@adonisjs/framework/src/Response')} Response */

/**
 * Resourceful controller for interacting with articlereservations
 */
class ArticleReservationController {

  constructor() {
    this.articleReservationService = new ArticleReservationService()
  }

  /**
   * Show a list of all articlereservations for current company.
   * GET articlereservations
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async index({request, auth, response}) {
    try {
      let reservations = await this.articleReservationService.all(auth.user);
      return response.json(reservations);
    }
    catch (e) {
      return response.status(422).json(e.message)
    }
  }

  /**
   * Create/save a new article.
   * POST articles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({request, auth, response}) {
    try {
      await this.articleReservationService.addReservation(auth.user, request.all());
      return response.created();
    }
    catch (e) {
      return response.status(422).json(e)
    }
  }

}

module.exports = ArticleReservationController
