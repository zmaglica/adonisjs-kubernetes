'use strict'
const User = use('App/Models/User')

class AuthController {

    async login({ request, auth, response }) {

        let { email, password } = request.all();

        try {
            if (await auth.attempt(email, password)) {
                let user = await User.findBy('email', email)
                let token = await auth.generate(user)
                return response.json(token)
            }
        }
        catch (e) {
            return response.status(422).json(e)
        }
    }
}

module.exports = AuthController
