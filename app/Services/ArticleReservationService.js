'use strict'

const ArticleReservation = use('App/Models/ArticleReservation')
const Article = use('App/Models/Article')
const Database = use('Database')

class ArticleReservationService {
  /**
   * Get all reservation for company or user
   *
   * @param user
   * @returns {Promise<void>}
   */
  async all(user) {
    if (user.company_id) {
      return await ArticleReservation
        .query()
        .select('article_reservations.*')
        .innerJoin('articles', 'articles.id', 'article_reservations.article_id')
        .innerJoin('products', function () {
          this
            .on('products.id', 'articles.product_id')
            .andOn('products.company_id', user.company_id)
        })
        .fetch();
    } else {
      return await user.articleReservations().fetch();
    }
  }

  /**
   * Add new reservation
   *
   * @param user
   * @param data
   * @returns {Promise<Response>}
   */
  async addReservation(user, data) {
    const article = await Article.findOrFail(data.article_id);
    let quantity = article.quantity;
    if (quantity < 1) {
      throw 'This item is currently not in stock'
    }
    quantity = quantity - 1;
    article.quantity = quantity
    await article.save()

    const articleReservation = new ArticleReservation();

    articleReservation.user_id = user.id
    articleReservation.article_id = data.article_id
    articleReservation.picking_date = data.picking_date

    await articleReservation.save()

  }
}

module.exports = ArticleReservationService
