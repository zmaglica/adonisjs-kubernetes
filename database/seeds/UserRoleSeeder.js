'use strict'

/*
|--------------------------------------------------------------------------
| UserRoleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const RoleModel = use('Role')

class UserRoleSeeder {
  async run () {
    const roles = [
      {
        'name': 'Administrator',
        'slug': 'administrator',
        'description': 'manage administration privileges'
      },{
        
        'name': 'Company',
        'slug': 'company',
        'description': 'manage company privileges'
      },
      {
        'name': 'Customer',
        'slug': 'customer',
        'description': 'manage customer privileges'
      }
    ];
    await RoleModel.createMany(roles);
  }
}

module.exports = UserRoleSeeder
