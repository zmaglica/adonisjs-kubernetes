'use strict'

/*
|--------------------------------------------------------------------------
| CompanySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Company = use('App/Models/Company')

class CompanySeeder {
  async run () {
    const compamy = new Company()

    compamy.name = "My Company";

    await compamy.save();
  }
}

module.exports = CompanySeeder
