'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Database = use('Database');
//const Hash = use('Hash'); Adonis hash passwords on presave event
const UserModel = use('App/Models/User')
class UserSeeder {
  async run() {

    const users = [
      {
        'company_id': 1,
        'username': 'company',
        'email': 'company@test.com',
        'password': 'test'
      },
      {
        'username': 'test',
        'email': 'test@test.com',
        'password': 'test'
      },
      {
        'username': 'test1',
        'email': 'test1@test.com',
        'password': 'test'
      },
      {
        'username': 'test2',
        'email': 'test2@test.com',
        'password': 'test'
      }
    ];

    // Add roles to the users
    let createdUsers = await UserModel.createMany(users)
    for(let user of createdUsers) {
      await user.roles().attach([(user.username === 'company') ? 2 : 3])
    }
  }
}

module.exports = UserSeeder
