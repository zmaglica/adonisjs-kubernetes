'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ArticlePropertyValueSchema extends Schema {
  up () {
    this.create('article_property_values', (table) => {
      table.integer('article_id').unsigned().notNullable().references('id').inTable('articles')
      table.integer('property_value_id').unsigned().notNullable().references('id').inTable('property_values')
    })
  }

  down () {
    this.drop('article_property_values')
  }
}

module.exports = ArticlePropertyValueSchema
