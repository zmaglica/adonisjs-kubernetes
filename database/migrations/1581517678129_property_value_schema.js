'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PropertyValueSchema extends Schema {
  up () {
    this.create('property_values', (table) => {
      table.increments()
      table.integer('property_id').unsigned().notNullable().references('id').inTable('properties')
      table.string('value', 80).notNullable()
      table.timestamps()
      table.datetime('deleted_at')
      table.unique(['property_id', 'value'])
    })
  }

  down () {
    this.drop('property_values')
  }
}

module.exports = PropertyValueSchema
