'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.increments()
      table.integer('company_id').unsigned().notNullable().references('id').inTable('companies')
      table.string('name', 80).notNullable()
      table.text('description').notNullable()
      table.timestamps()
      table.datetime('deleted_at')
      table.unique(['company_id', 'name'])
    })
  }

  down () {
    this.drop('products')
  }
}

module.exports = ProductSchema
