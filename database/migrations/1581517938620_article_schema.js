'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ArticleSchema extends Schema {
  up () {
    this.create('articles', (table) => {
      table.increments()
      table.integer('product_id').unsigned().notNullable().references('id').inTable('products')
      table.string('name', 80).notNullable()
      table.string('sku', 80).notNullable()
      table.decimal('price', 14, 2).notNullable()
      table.integer('quantity').notNullable()
      table.timestamps()
      table.datetime('deleted_at')
    })
  }

  down () {
    this.drop('articles')
  }
}

module.exports = ArticleSchema
