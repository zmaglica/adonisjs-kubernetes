'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ArticleReservationSchema extends Schema {
  up () {
    this.create('article_reservations', (table) => {
      table.increments()
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users')
      table.integer('article_id').unsigned().notNullable().references('id').inTable('articles')
      table.date('picking_date').notNullable()
      table.timestamps()
      table.datetime('deleted_at')
    })
  }

  down () {
    this.drop('article_reservations')
  }
}

module.exports = ArticleReservationSchema
