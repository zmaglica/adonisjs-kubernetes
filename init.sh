source .env
if [[ "$NODE_ENV" == "production" ]]; then
  npm install --only=prod
	adonis serve
else
  npm install
	adonis serve --dev
fi
