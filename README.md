# Adonis CRUD with Kubernetes and CI/CD pipeline

Simple CRUD application based on AdonisJS with AWS EKS and Gitlab Ci/CD pielines

## Installation

**Using Docker**

Step 1. Install [Docker](https://docs.docker.com/install/)

Step 2. Create .env file from .env.example

```bash
cp .env.example .env
```
Step 3. Generate a new Application KEY

```bash
adonis key:generate
```
Step 4. Edit .env file according to your project needs like application name, port, ...

Step 5. Build a Docker image
```bash
docker-compose build
```
Step 6. Execute command to run docker containers
```bash
docker-compose up -d
```
**Without Docker**

Step 1. Create .env file from .env.example

```bash
cp .env.example .env
```
Step 2. Edit .env file according to your project needs like application name, port, ...


Step 3. Install dependencies
```bash
npm install
```
Step 4. Generate a new Application KEY

```bash
adonis key:generate
```
Step 5. Start adonis server

```bash
adonis serve --dev
```


## Usage
Execute following command to create a database with sample data


```nodejs
migration:run --seed
```
You can use the following user credentials to consume API.
```nodejs
company@test.com
test@test.com
test1@test.com
test2@test.com

The default password for all users is 'test'

```
## API Usage


**Product**

In a real-life scenario, we can have one product with several different options that can affect price and quantity. Let's assume that we are selling SSD drive and the capacity of that drive can be different (256 MB, 512 MB, ..). The only difference between these products is capacity and to sell it on our webshop we need to create a different product for each SSD drive. The problem with this approach is if we want to change something (like description of SSD or some parameter) we need to find every possible drive with the same product specs and update it accordingly. 

**Article**

To avoid this issue you can create a new "Article". The article is a simple extension of our product. Now we can create a Product and add all specifications, descriptions in a single row in database and differences like size, capacity, quantity or any other property can be addressed directly on Article.


Ok now we solved the problem above but what if we want to use the "capacity" property of our SSD drive to other products (like RAM memory). That's why CRUD operations are added for adding new Property and Property Value.

**Property**

In property, we can store every possible property that one Article can have. As an example, our website can sell t-shirts. One t-shirt can have many colors and sizes. To make this process easier, we can create a new "Property" and attach it to the Article. For our example let's create a property "Color" which will be used for t-shirt Articles

**Property Value**
Let's go back to our t-shirt company where we created a property named "Color". In a real-life scenario, one t-shirt can have different colors and sizes. To make everything easier you can add as many values for a particular property that can be used for our Article.

**Real life example with steps**

Step 1. Let's create a new product "T-Shirt"

Step 2. Now let's add some properties that can be linked to our product/articles. We will create a new "Property" with names "Color" and "Size"

Step 3. It is time to add value to the properties. For "Color" property we can add multiple colors (Red, Green, Blue) and for "Size" property we can add following values "Small", "Medium", "Large",

Step 4. Now we can create an Article that we can sell to customers. Let's create a new Article with the name "Blue Polo t-shirt". After that, we can attach Properties with their value and specify the price and quantity that can be sold.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)